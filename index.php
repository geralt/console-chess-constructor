<?php
require 'vendor/autoload.php';

use Classes\Game;

$game = new Game();
$game->run();