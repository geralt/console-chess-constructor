<?php

namespace Classes;

use Interfaces\IQuestion;

class Question implements IQuestion
{
    protected $board;
    protected $lastQuestion;

    public function __construct()
    {
        $this->board = new Board();
    }

    public function askQuestion($question = '', $needSave = false)
    {
        if ($needSave) {
            $this->lastQuestion = $question;
        }
        echo $question;
        return $this->readUserAnswer();
    }

    public function notify($message)
    {
        echo $message . "\n";
    }

    public function readUserAnswer()
    {
        return trim(fgets(STDIN));
    }

    public function askBoardSize()
    {
        $changeBoardSize = $this->askQuestion('Would you like to change default board size (8x8) ? Yes or no: ');
        if ('yes' === strtolower($changeBoardSize) || 'y' === strtolower($changeBoardSize)) {
            $boardSizeX = 0;
            $boardSizeY = 0;
            while (false === $this->board->checkHorizontal($boardSizeX) || false === $this->board->checkHorizontal($boardSizeY)) {
                list($boardSizeX, $boardSizeY) = $this->askSetNewBoardSize();
            }
            $this->board->setBoardSize($boardSizeX, $boardSizeY);
        } else {
            $this->board->setBoardSize($this->board->getBoardSizeX(), $this->board->getBoardSizeY());
        }

    }

    public function chooseAction()
    {
        $action = $this->askQuestion("Choose action: \n
        b - change board size\n
        t - create new figure type\n
        f - create new figure\n
        m - move figure\n
        s - save\n
        l - load\n
        v - view figures\n
        r - create common report after figure created\n
        c - change default storage\n
        another key - exit\n");
        switch ($action) {
            case 'b':
                $this->askBoardSize();
                break;
            case 't':
                $this->askCreateFigureType();
                break;
            case 'f':
                $this->askCreateFigure();
                break;
            case 'm':
                $this->askMove();
                break;
            case 's':
                $this->saveBoard();
                break;
            case 'l':
                $this->loadBoard();
                break;
            case 'v':
                $this->askViewBoard();
                break;
            case 'r':
                $this->askReport();
                break;
            case 'c':
                $this->changeStorage();
                break;
            default:
                break;
        }
    }

    public function askSetNewBoardSize()
    {
        $boardSize = $this->askQuestion('Enter board size. Separate values by "x": ');
        list($boardSizeX, $boardSizeY) = explode('x', $boardSize);
        return [$boardSizeX, $boardSizeY];
    }

    public function askCreateFigureType()
    {
        $needNewFigure = $this->askQuestion('Do you want to create new figure type? Yes or no: ', true);
        if ('yes' === strtolower($needNewFigure) || 'y' === strtolower($needNewFigure)) {
            $newFigure = $this->askQuestion('Type it\'s name: ');
            while (true === $this->board->checkIsFigureNameExists($newFigure)) {
                $this->notify('This name is already exists! Select another one');
                $newFigure = $this->askQuestion('Type it\'s name: ');
            }
            $abbreviation = $this->askQuestion('It\'s abbreviation: ');
            while (true === $this->board->checkIsAbbreviationExists($abbreviation)) {
                $this->notify('This abbreviation is already exists! Select another one');
                $abbreviation = $this->askQuestion('It\'s abbreviation: ');
            }
            $needMessage = $this->askQuestion('Do you want to specify message for this figure type? Yes or no: ');
            $customMessage = '';
            if ('yes' === strtolower($needMessage) || 'y' === strtolower($needMessage)) {
                $customMessage = $this->askQuestion('Type it: ');
            }
            $this->board->createFigureType($newFigure, $abbreviation, $customMessage);
            $this->notify('Created successfully');
        }
        $this->chooseAction();
    }

    public function askCreateFigure()
    {
        $needNewFigure = $this->askQuestion('Do you want to create new figure? Yes or no: ', true);
        if ('yes' === strtolower($needNewFigure) || 'y' === strtolower($needNewFigure)) {
            $type = $this->askChooseFigureType();
            $color = $this->askQuestion('Type it\'s color: b - for black; w - for white: ');;
            while ($color !== 'b' && $color !== 'w') {
                $this->notify('Wrong color! Choose white (w) or black (b)!');
                $color = $this->askQuestion('Type it\'s color: b - for black; w - for white: ');
            }
            $field = $this->askField();
            $type = $this->board->getFigureTypeByAbbreviation($type);
            $this->board->createFigure($color, $field, $type);
            if ($type->getCustomFigureTypeCreationMessage()) {
                $this->notify($type->getCustomFigureTypeCreationMessage() . "\n");
            }

            if($type->getCommonFigureTypeCreationMessage()) {
                $this->notify($type->getCommonFigureTypeCreationMessage() . "\n");
            }
        }
        $this->chooseAction();
    }

    public function askViewBoard()
    {
        $viewBoard = $this->askQuestion('Do you want to view the board? Yes or no: ');
        if ('yes' === strtolower($viewBoard) || 'y' === strtolower($viewBoard)) {
            $this->notify($this->board->viewFigures());
        }
        $this->chooseAction();
    }

    public function askChooseFigureType()
    {
        $types = $this->board->getFiguresTypes();
        $typesAsString = "Put follow char to select:\n";
        foreach ($types as $type) {
            $typesAsString .= $type->getTypeAbbreviation() . ' - ' . $type->getTypeName() . "\n";
        }
        $figureType = '';
        while (false === $this->board->isValidFigureType($figureType)) {
            $figureType = $this->askQuestion($typesAsString);
        }
        return $figureType;
    }

    public function askMove()
    {
        $moveFigure = $this->askQuestion('Do you want to move figure? Yes or no: ');
        if('yes' === strtolower($moveFigure) || 'y' === strtolower($moveFigure)) {
            $fromField = $this->askField('Select the first field: ');
            list($color, $key) = $this->board->findFigure($fromField);
            if($color) {
                $toField = $this->askField('Select the second field: ');
                $this->board->moveFigure($fromField, $toField);
                $this->notify('Moved successfully');
            } else {
                $this->notify('No figure on this field');
            }
        }
        $this->chooseAction();
    }

    protected function askField($question = 'Select the field: ')
    {
        $field = $this->askQuestion($question);
        while (false === $this->board->checkField($field)) {
            $this->notify('Wrong field! Choose correct field. I remind you, the board size is ' . $this->board->getBoardSize());
            $field = $this->askQuestion($question);
        }
        return $field;
    }

    public function saveBoard()
    {
        $this->board->saveBoard();
        $this->chooseAction();
    }

    public function loadBoard()
    {
        $this->board->loadBoard();
        $this->notify($this->board->getBoardSize());
        $this->notify($this->board->viewFigures());
        $this->chooseAction();
    }

    public function askReport()
    {
        $needReport = $this->askQuestion('Do you want to specify common message? Yes or no: ');
        if('yes' === strtolower($needReport) || 'y' === strtolower($needReport)) {
            $report = $this->askQuestion('Type it: ');
            $this->board->setFigureTypeCommonMessage($report);
        }
        $this->chooseAction();
    }

    public function changeStorage()
    {
        $needChangeStorage = $this->askQuestion('Do you want to change default storage? Yes or no: ');
        if('yes' === strtolower($needChangeStorage) || 'y' === strtolower($needChangeStorage)) {
            $storage = $this->askQuestion('Choose one: file or redis:');
            while(!in_array($storage, ['file', 'redis'])) {
                $storage = $this->askQuestion('Choose one: file or redis:');
            }
            $manager = new DatabaseManager();
            $manager->setDatabase($storage);

            $this->board->setDatabaseManager($manager);
        }
        $this->chooseAction();
    }
}