<?php

namespace Classes;

use Config\Database;
use Classes\Database\File;
use Classes\Database\Redis;

class DatabaseManager
{
    private $possibleDbs = ['file', 'redis'];
    private $currentDb = 'file';
    private $settings  = [];
    private $config;
    public $dbInstance;

    public function __construct()
    {
        $this->config = new Database();
        $this->setDatabase($this->currentDb);
    }

    public function readSettings()
    {
        switch ($this->currentDb) {
            case 'file':
                $this->settings = $this->config->getFileSettings();
                break;
            case 'redis':
                $this->settings = $this->config->getRedisSettings();
                break;
            default:
                $this->settings = $this->config->getFileSettings();
        }

        return $this->settings;

    }

    public function setDatabase($dbName)
    {
        if(in_array($dbName, $this->possibleDbs)) {
            $this->setCurrentDb($dbName);
            $this->readSettings();
            $this->getInstance();
        }
    }

    public function getInstance()
    {
        switch ($this->currentDb) {
            case 'file':
                $this->dbInstance = File::getInstance();
                break;
            case 'redis':
                $this->dbInstance = Redis::getInstance();
                break;
            default:
                $this->dbInstance = File::getInstance();
        }
        $this->dbInstance->connect($this->settings);
    }

    public function read()
    {
        return $this->dbInstance->read();
    }

    public function write($data)
    {
        return $this->dbInstance->write($data);
    }

    public function getCurrentDb()
    {
        return $this->currentDb;
    }

    public function setCurrentDb($db)
    {
        $this->currentDb = $db;
    }
}