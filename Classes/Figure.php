<?php

namespace Classes;

use Classes\Figure\Type;
use Interfaces\IFigure;

class Figure implements IFigure
{
    protected $coordinates;
    protected $type;
    protected $color;

    public function __construct(string $color, string $field, Type $type)
    {
        $this->setColor($color)
            ->setCoordinates($field)
            ->setType($type);
    }

    public function getColor()
    {
        return $this->color;
    }

    public function getCoordinates()
    {
        return $this->coordinates;
    }

    public function getName()
    {
        return $this->type->getTypeName();
    }

    public function getAbbreviation()
    {
        return $this->type->getTypeAbbreviation();
    }

    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }

    public function setCoordinates(string $coord)
    {
        $this->coordinates = $coord;
        return $this;
    }

    public function setType(Type $type)
    {
        $this->type = $type;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function __toString()
    {
        $data = [
            'name' => $this->getName(),
            'abbreviation' => $this->getAbbreviation(),
            'coordinates' => $this->getCoordinates(),
            'color' => $this->getColor()
        ];
        return json_encode($data);
    }
}