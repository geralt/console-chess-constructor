<?php

namespace Classes\Database;

use Interfaces\IDatabase;
use Predis\Autoloader;
use Predis\Client;


class Redis implements IDatabase
{
    private static $_instance = null;
    private $client;

    protected function __clone() {

    }

    static public function getInstance() {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct()
    {
        Autoloader::register();
    }

    public function connect($settings)
    {
        $this->client = new Client($settings);
    }

    public function read()
    {
        return $this->client->get('board');
    }

    public function write($data)
    {
        $this->client->set('board', $data);
    }
}