<?php
namespace Classes\Database;

use Interfaces\IDatabase;

class File implements IDatabase
{
    private $file;

    private static $_instance = null;

    protected function __clone() {

    }

    static public function getInstance() {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct()
    {

    }

    public function connect($settings)
    {
        $this->file = $settings['path'];
    }

    public function read()
    {
        return file_get_contents($this->file);
    }

    public function write($data)
    {
        return file_put_contents($this->file, $data);
    }
}