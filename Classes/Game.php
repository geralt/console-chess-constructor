<?php

namespace Classes;

use Interfaces\IGame;

class Game implements IGame
{
    public $questions;

    public function __construct()
    {
        $this->questions = new Question();
    }

    public function run()
    {
        $this->questions->chooseAction();
        $this->finish();
    }

    public function finish()
    {
        echo 'Bye';
    }
}