<?php

namespace Classes\Figure;

use Interfaces\Figure\IType;

class Type implements IType
{
    protected $name;
    protected $abbreviation;
    protected $customFigureTypeCreationMessage = [];
    protected $commonFigureTypeCreationMessage = '';

    public function __construct(string $name, string $abbreviation)
    {
        $this->setTypeName($name)
            ->setTypeAbbreviation($abbreviation);
    }

    public function setTypeName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setTypeAbbreviation($abbreviation)
    {
        $this->abbreviation = $abbreviation;
        return $this;
    }

    public function getTypeName()
    {
        return $this->name;
    }

    public function getTypeAbbreviation()
    {
        return $this->abbreviation;
    }

    public function __toString()
    {
        $data = [
            'name' => $this->getTypeName(),
            'abbreviation' => $this->getTypeAbbreviation()
        ];
        return json_encode($data);
    }

    public function setCustomFigureTypeCreationMessage($abbreviation, $message)
    {
        $this->customFigureTypeCreationMessage[$abbreviation] = $message;
        return $this;
    }

    public function setCommonFigureTypeCreationMessage($message)
    {
        $this->commonFigureTypeCreationMessage = $message;
        return $this;
    }

    public function getCustomFigureTypeCreationMessage()
    {
        if(array_key_exists($this->abbreviation, $this->customFigureTypeCreationMessage)) {
            return $this->customFigureTypeCreationMessage[$this->abbreviation];
        }
        return false;
    }

    public function getCommonFigureTypeCreationMessage()
    {
        return $this->commonFigureTypeCreationMessage;
    }
}