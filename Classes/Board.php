<?php

namespace Classes;

use Classes\Figure\Type;
use Interfaces\IBoard;

/**
 * Class Board
 * За возможные размеры доски примем английский алфавит, т.к. обычно доски подписаны по горизонтали буквами, а по
 * вертикали цифрами.
 * @package Classes
 */
class Board implements IBoard
{
    private $possibleSizes = [];
    private $boardSizeX = 8;
    private $boardSizeY = 8;
    private $figuresTypes = [];
    private $figures = [
        'w' => [],
        'b' => []
    ];
    private $databaseManager;
    public $commonMessage = '';

    /**
     * Board constructor.
     * Заполняем возможные размеры
     */
    public function __construct()
    {
        $this->databaseManager = new DatabaseManager();
        $i = 1;
        foreach(range('a','z') as $char) {
            $this->possibleSizes[$char] = $i;
            $i++;
        }
    }

    public function getBoardSize()
    {
        return $this->boardSizeX . 'x' . $this->boardSizeY;
    }

    public function getBoardSizeX()
    {
        return $this->boardSizeX;
    }

    public function getBoardSizeY()
    {
        return $this->boardSizeY;
    }

    public function setBoardSize(int $x, int $y)
    {
        $this->boardSizeX = $x;
        $this->boardSizeY = $y;
        return $this;
    }

    public function createFigureType(string $name, string $abbreviation, string $customMessage = ''): Figure\Type
    {
        $type = new Type($name, $abbreviation);
        if($customMessage) {
            $type->setCustomFigureTypeCreationMessage($abbreviation, $customMessage);
        }
        $this->addFigureType($type);
        $this->applyCommonMessage();
        return $type;
    }

    public function addFigureType(Type $type)
    {
        $this->figuresTypes[] = $type;
    }

    public function getFiguresTypes()
    {
        return $this->figuresTypes;
    }

    public function getFigures()
    {
        return $this->figures;
    }

    public function createFigure(string $color, string $field, Figure\Type $type)
    {
        list($_color, $key) = $this->findFigure($field);
        if($_color) {
            $this->removeFigure($_color, $key);
        }
        $figure = new Figure($color, $field, $type);
        $this->figures[$color][] = $figure;
        return $figure;
    }

    public function removeFigure($color, $key)
    {
        unset($this->figures[$color][$key]);
    }

    public function checkField(string $field)
    {
        $letter = $field[0];
        $isVerticalValid = $this->checkVertical($letter);
        $horizontal = (int)substr($field, 1);
        $isHorizontalValid = $this->checkHorizontal($horizontal);

        return $isVerticalValid && $isHorizontalValid;
    }

    /**
     * Проверяет, возможна ли указанная вертикаль
     * если указанная буква вертикали есть в возможных размерах, то да
     * @param $letter
     * @return bool
     */
    public function checkVertical($letter)
    {
        if(array_key_exists($letter, $this->possibleSizes)) {
            return true;
        }
        return false;
    }

    /**
     * Проверяет, возможна ли указанная горизонталь
     * если число больше, чем значение последнего элемента массива, то нет
     * @param $number
     * @return bool
     */
    public function checkHorizontal(int $number)
    {
        if($number > 0 && $number <= $this->boardSizeY) {
            return true;
        }
        return false;
    }

    public function checkIsFieldEngaged($field)
    {
        if($this->findFigure($field)) {
            return true;
        }
        return false;
    }


    public function moveFigure($startField, $finishField)
    {
        list($color, $key) = $this->findFigure($startField);
        list($color2, $key2) = $this->findFigure($finishField);
        if($color2) {
            $this->removeFigure($color2, $key2);
        }
        $figure = $this->figures[$color][$key];
        $figure->setCoordinates($finishField);
    }

    public function isValidFigureType($abbreviation)
    {
        foreach ($this->getFiguresTypes() as $type) {
            if($type->getTypeAbbreviation() === $abbreviation) {
                return true;
            }
        }
        return false;
    }

    public function getFigureTypeByAbbreviation($abbreviation)
    {
        foreach ($this->getFiguresTypes() as $type) {
            if($type->getTypeAbbreviation() === $abbreviation) {
                return $type;
            }
        }
        return false;
    }

    public function checkIsFigureNameExists($name)
    {
        foreach ($this->getFiguresTypes() as $type) {
            if($type->getTypeName() === $name) {
                return true;
            }
        }
        return false;
    }

    public function checkIsAbbreviationExists($abbreviation)
    {
        foreach ($this->getFiguresTypes() as $type) {
            if($type->getTypeAbbreviation() === $abbreviation) {
                return true;
            }
        }
        return false;
    }

    public function findFigure(string $field)
    {
        foreach ($this->getFigures() as $color => $figures) {
            foreach ($figures as $key => $figure) {
                if($figure->getCoordinates() === $field) {
                    return [$color, $key];
                }
            }
        }
        return false;
    }

    public function viewFigures()
    {
        $board = '';
        foreach ($this->getFigures() as $color => $figures) {
            if($color === 'w') {
                $board .= "White:\n";
            }
            if($color === 'b') {
                $board .= "Black: \n";
            }
            foreach ($figures as $figure) {
                $board .= $figure->getAbbreviation() . $figure->getCoordinates() . "\n";
            }
        }
        return $board;
    }

    public function saveBoard()
    {
        $data = [];
        $data['boardSizeX'] = $this->getBoardSizeX();
        $data['boardSizeY'] = $this->getBoardSizeY();
        $data['figuresTypes'] = [];// = $this->getFiguresTypes();
        $data['figures'] = [];//$this->getFigures();
        foreach ($this->getFiguresTypes() as $type) {
            $data['figuresTypes'][] = (string)$type;
        }
        foreach ($this->getFigures() as $color => $figures) {
            $data['figures'][$color] = [];
            foreach ($figures as $figure) {
                $data['figures'][$color][] = (string)$figure;
            }
        }
        $this->databaseManager->write(json_encode($data));
    }

    public function loadBoard()
    {
        $data = json_decode($this->databaseManager->read());
        if(isset($data->boardSizeX)) {
            $this->setBoardSize($data->boardSizeX, $data->boardSizeY);
            foreach ($data->figuresTypes as $type) {
                $type = json_decode($type);
                $this->createFigureType($type->name, $type->abbreviation);
            }
            foreach ($data->figures as $color => $figures) {
                foreach ($figures as $figure) {
                    $figure = json_decode($figure);
                    $type = $this->getFigureTypeByAbbreviation($figure->abbreviation);
                    $this->createFigure($color, $figure->coordinates, $type);
                }
            }
        }
    }

    public function setFigureTypeCommonMessage($message)
    {
        $this->commonMessage = $message;
    }

    public function applyCommonMessage()
    {
        foreach ($this->getFiguresTypes() as $figuresType) {
            $figuresType->setCommonFigureTypeCreationMessage($this->commonMessage);
        }
    }

    public function setDatabaseManager($manager)
    {
        $this->databaseManager = $manager;
    }
}