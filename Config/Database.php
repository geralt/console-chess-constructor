<?php

namespace Config;

use Dotenv\Dotenv;

class Database
{
    private $dotenv;

    public function __construct()
    {
        $this->dotenv = new Dotenv(__DIR__ . '/../');
        $this->dotenv->load();
    }

    public function getFileSettings()
    {
        return [
            'path' => __DIR__ . '/../storage.txt'
        ];
    }

    public function getRedisSettings()
    {
        return [
            'host' => getenv('REDIS_HOST', '127.0.0.1'),
            //'password' => getenv('REDIS_PASSWORD', null),
            'port' => getenv('REDIS_PORT', 6379),
            'database' => null,
        ];
    }
}