<?php

namespace Tests\Unit;

use Classes\Board;
use PHPUnit\Framework\TestCase;

class FigureBehaviorTest extends TestCase
{
    public $board;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->board = new Board();
    }

    public function testIsValidField()
    {
        $this->assertEquals(false, $this->board->checkField('a0'));
        $this->assertEquals(false, $this->board->checkField('01'));
        $this->assertEquals(true, $this->board->checkField('h8'));
        $this->assertEquals(false, $this->board->checkField('h9'));
    }

    public function testFigureTypeCreatedTest()
    {
        $this->assertCount(0, $this->board->getFiguresTypes());
        $this->board->createFigureType('Rook', 'R');
        $this->assertCount(1, $this->board->getFiguresTypes());
    }

    public function testFigureCreatedTest()
    {
        $this->assertCount(0, $this->board->getFigures()['w']);
        $type = $this->board->createFigureType('Rook', 'R');
        $this->board->createFigure('w', 'a1', $type);
        $this->assertCount(1, $this->board->getFigures()['w']);
        $this->assertEquals('a1', $this->board->getFigures()['w'][0]->getCoordinates());
    }

    public function testIfFigureMoved()
    {
        $type = $this->board->createFigureType('Rook', 'R');
        $this->board->createFigure('w', 'a1', $type);
        $this->board->createFigure('b', 'a2', $type);
        $this->assertCount(1, $this->board->getFigures()['w']);
        $this->assertCount(1, $this->board->getFigures()['b']);
        $this->assertEquals('a1', $this->board->getFigures()['w'][0]->getCoordinates());
        $this->assertEquals('a2', $this->board->getFigures()['b'][0]->getCoordinates());
        $this->board->moveFigure('a1', 'a2');
        $this->assertEquals('a2', $this->board->getFigures()['w'][0]->getCoordinates());
        $this->assertCount(0, $this->board->getFigures()['b']);
    }
}
