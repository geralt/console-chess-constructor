<?php

namespace Interfaces\Figure;

interface IType
{
    public function setTypeName($name);

    public function setTypeAbbreviation($abbreviation);

    public function getTypeName();

    public function getTypeAbbreviation();

    public function setCustomFigureTypeCreationMessage($abbreviation, $message);

    public function setCommonFigureTypeCreationMessage($message);

    public function getCustomFigureTypeCreationMessage();

    public function getCommonFigureTypeCreationMessage();
}