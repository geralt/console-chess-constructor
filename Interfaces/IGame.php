<?php

namespace Interfaces;

interface IGame
{
    public function run();

    public function finish();
}