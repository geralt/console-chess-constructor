<?php

namespace Interfaces;

use Classes\Figure;

interface IBoard
{
    public function getBoardSize();

    public function setBoardSize(int $x, int $y);

    public function createFigureType(string $name, string $abbreviation, string $customMessage = ''): Figure\Type;

    public function createFigure(string $color, string $field, Figure\Type $type);

    public function moveFigure($startField, $finishField);

    public function checkField(string $field);

    public function findFigure(string $field);

    public function addFigureType(Figure\Type $type);

    public function isValidFigureType($abbreviation);

    public function getFigureTypeByAbbreviation($abbreviation);

    public function saveBoard();

    public function loadBoard();
}