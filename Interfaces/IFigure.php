<?php

namespace Interfaces;

use Classes\Figure\Type;

interface IFigure
{
    public function getCoordinates();

    public function getName();

    public function getAbbreviation();

    public function setCoordinates(string $field);

    public function setType(Type $type);

    public function getType();
}