<?php

namespace Interfaces;

interface IQuestion
{
    public function askQuestion($question);

    public function readUserAnswer();
}