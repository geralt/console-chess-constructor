<?php

namespace Interfaces;

interface IDatabase
{
    public function connect($settings);

    public function read();

    public function write($data);
}